import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CountryComponent } from './country/country.component';
import { CityComponent } from './city/city.component';
import { DepartmentComponent } from './department/department.component';

const routes: Routes = [
  { path: 'city', component: CityComponent },
  { path: 'department', component: DepartmentComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
