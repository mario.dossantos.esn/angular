import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'tp_3_composants';

  weatherType = ""

  changeWeatherDisplay(city: string) {
    if (city == "Paris") this.weatherType = "Ciel bleu"
    if (city == "London") this.weatherType = "Pluie"
  }
}
