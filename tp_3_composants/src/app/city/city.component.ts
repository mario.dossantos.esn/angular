import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() cityName!: string;

  @Output() newCityEvent = new EventEmitter<string>();

  sendCityName() {
    this.newCityEvent.emit(this.cityName);
  }

}
