import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tp_6_directives';

  // --- PART 1

  fontSize = 60
  fontSizePX = `${this.fontSize}px`

  changeSize(value: number) {
    this.fontSize += value
    this.updatePixelSize()
  }

  updatePixelSize() {
    this.fontSizePX = `${this.fontSize}px`
  }

  // --- PART 2

  runners = [
    "Michel",
    "John",
    "Toto"
  ]
}
