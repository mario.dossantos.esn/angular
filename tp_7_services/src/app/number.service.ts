import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NumberService {

  constructor() { }

  isNumerical(num: any) {
    return !isNaN(num)
  }

  isMulitpleOfFive(num: any) {
    return (num % 5 === 0)
  }
}
