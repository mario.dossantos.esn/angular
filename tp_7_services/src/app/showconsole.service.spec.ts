import { TestBed } from '@angular/core/testing';

import { ShowconsoleService } from './showconsole.service';

describe('ShowconsoleService', () => {
  let service: ShowconsoleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShowconsoleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
