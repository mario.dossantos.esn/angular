import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShowconsoleService {

  constructor() { }

  log(value: any) {
    console.log(value)
  }
}
