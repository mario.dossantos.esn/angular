import { Component } from '@angular/core';
import { ShowconsoleService } from './showconsole.service';
import { NumberService } from './number.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'tp_7_services';

  constructor(private NumberService: NumberService, private ShowConsoleService: ShowconsoleService) { }

  updateValue(value: any) {
    this.ShowConsoleService.log(
      `La valeur saisie '${value}' est un ${this.NumberService.isNumerical(value) ? "nombre" : "hexa"}.`
    )

    this.ShowConsoleService.log(
      `La valeur saisie '${value}' ${this.NumberService.isMulitpleOfFive(value) ? "est" : "n'est pas"} un multiple de 5.`
    )
  }
}
