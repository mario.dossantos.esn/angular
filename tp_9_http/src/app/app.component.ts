import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tp_10_http'

  private wsUrl: string = "https://jsonplaceholder.typicode.com/posts";

  constructor(private http: HttpClient) { }

  executeHTTPClientPost() {
    this.http.get(this.wsUrl).subscribe({
      next: this.handleUpdateResponse.bind(this),
      error: this.handleError.bind(this)
    })
  }

  handleUpdateResponse(res: any) {
    for (let key in res) {
      console.log('Title :  ', key, res[key]['title']);
    }
  }

  handleError(err: any) {
    console.log('KO : ', err);
  }
}
