import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tp_4_binding';

  // --- PART 1

  bgColor = "teal"

  switchColor() {
    this.bgColor = this.bgColor == "teal" ? "tomato" : "teal"
  }

  // --- PART 2

  defaultFormValue = "Pouet pouet"

}
