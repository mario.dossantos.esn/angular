import { Component } from '@angular/core';
import City from './classes/City'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tp_5_classes';

  cities = [
    new City("Castres", 40000),
    new City("Mazamet", 100),
    new City("Paris", 1),
  ]
}
