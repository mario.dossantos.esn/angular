export default class City {
    name: string;
    population: number;

    constructor(name: string, population: number) {
        this.name = name
        this.population = population
    }

    displayMessage() {
        return `${this.name} comporte ${this.population} habitant${this.population > 1 ? "s" : ""}.`
    }
}